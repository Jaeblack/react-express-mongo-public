import Axios from 'axios'
import React, { useState } from 'react'
import Swal from 'sweetalert2'

export default function Formulario() {

    const [nombre, setNombre] = useState('')
    const [apellido, setApellido] = useState('')
    const [salario, setSalario] = useState('')

    const registrar = async(e)=>{
        e.preventDefault()
        const NuevoEmpleado = {nombre, apellido, salario}
        const URLpost = 'https://dvs-test.herokuapp.com/api/';
        //const URLpost ='localhost:4000/api'
        try{
            const respuesta = await Axios.post(URLpost, NuevoEmpleado)
       
            const mensaje = (
                'Nombre: '+ respuesta.data.valor.nombre+
                '\nApellido: '+respuesta.data.valor.apellido+
                '\nSalario: '+respuesta.data.valor.salario+
                '\nGuardado con el ID: '+respuesta.data.valor._id
            );
            Swal.fire({
                icon: 'success',
                title: mensaje,
                showConfirmButton: false,
                timer: 5000
              })
        } catch (errr){
            console.log(errr);
        }
        
    }

    return (
        <div className="container col-md-3 mt-4" >
            <form onSubmit={registrar}>
                <div className="mb-3" >
                    <input type="text" className="form-control" required placeholder="Nombre" onChange ={
                        e=>setNombre(e.target.value)
                    }/>            
                </div>
                <div className="mb-3">
                    <input type="text" className="form-control" required placeholder="Apellido" onChange ={
                        e=>setApellido(e.target.value)
                    } />
                </div>
                <div className="mb-3">
                    <input type="text" className="form-control" required placeholder="Salario" onChange ={
                        e=>setSalario(e.target.value)
                    } />
                </div>
                
                <button type="submit" className="btn btn-primary">Guardar</button>
            </form>
        </div>
    )
}